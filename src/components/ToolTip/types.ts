export type TPlacement = "top" | "right" | "bottom" | "left";

export interface ICoords {
    top: number;
    left: number;
    arrow: {
        left?: number | string;
        bottom?: number | string;
        top?: number | string;
        right?: number | string;
    };
}

export interface IReturn {
    topOffset: number;
    leftOffset: number;
    position: TPlacement;
    arrow: {
        left?: number | string;
        bottom?: number | string;
        top?: number | string;
        right?: number | string;
    }
}

interface IArrowConfig {
    arrowStyles: CSSStyleDeclaration,
    arrowSideSize: number;
    arrowOffset: number;
    arrowHalfSize: number;
}

interface IChildRect {
    topSpace: number;
    leftSpace: number;
    childWidth: number;
    childHalfWidth: number;
    childHeight: number;
    childHalfHeight: number;
}

interface ITooltipSizes {
    tooltipBodyWidthWithArrow: number;
    tooltipBodyHeightWithArrow: number;
    tooltipBodyHeight: number;
    tooltipBodyWidth: number;
    childOffset: number;
}

export interface IInitialValues {
    resultPosition: TPlacement;
    topOffset: number;
    leftOffset: number;
    arrowPosition: IReturn['arrow'];
    arrowConfig: IArrowConfig;
    childRect: IChildRect;
    tooltipSizes: ITooltipSizes;
}
