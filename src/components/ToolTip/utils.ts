import {IInitialValues, IReturn, TPlacement} from "./types";

interface IGetCoordsProps {
    child: HTMLElement;
    content: HTMLElement;
    arrow: HTMLElement;
}


const getSquareDiagonal = (sideSize: number): number => {
    const sqrValue = Math.sqrt(2);
    return sqrValue * sideSize;
}
const getArrowSizes = (arrow: HTMLElement) => {
    const arrowStyles = getComputedStyle(arrow);

    const arrowSideSize = parseInt(arrowStyles.width);
    const arrowHalfSize = arrowSideSize / 2;
    const arrowOffset = getSquareDiagonal(arrowSideSize) / 2;

    return {
        arrowOffset,
        arrowHalfSize,
        arrowStyles,
        arrowSideSize
    }
}

export const getTooltipContentSize = (tooltipContent: HTMLElement) => {
    const width = tooltipContent.offsetWidth;
    const height = tooltipContent.offsetHeight;

    return {
        height,
        width
    }
}

const getInitialValues = (placement: TPlacement, props: IGetCoordsProps): IInitialValues => {
    const {arrowSideSize, arrowHalfSize, arrowStyles, arrowOffset} = getArrowSizes(props.arrow);
    const {
        top: topSpace,
        left: leftSpace,
        width: childWidth,
        height: childHeight
    } = props.child.getBoundingClientRect();

    const {width: tooltipBodyWidth, height: tooltipBodyHeight} = getTooltipContentSize(props.content);

    let tooltipBodyWidthWithArrow = tooltipBodyWidth;
    let tooltipBodyHeightWithArrow = tooltipBodyHeight;
    let childOffset = 0;

    if (placement === 'top' || placement === 'bottom') {
        tooltipBodyHeightWithArrow += arrowOffset;
        childOffset = Math.ceil((tooltipBodyWidth - childWidth) / 2);
    } else {
        tooltipBodyWidthWithArrow += arrowOffset;
        childOffset = Math.ceil((tooltipBodyHeight - childHeight) / 2);
    }

    return {
        resultPosition: placement,
        topOffset: 0,
        leftOffset: 0,
        arrowPosition: {},
        arrowConfig: {
            arrowOffset,
            arrowHalfSize,
            arrowStyles,
            arrowSideSize
        },
        childRect: {
            topSpace,
            leftSpace,
            childHeight,
            childWidth,
            childHalfHeight: childHeight / 2,
            childHalfWidth: childWidth / 2,
        },
        tooltipSizes: {
            tooltipBodyWidthWithArrow,
            tooltipBodyHeightWithArrow,
            tooltipBodyHeight,
            tooltipBodyWidth,
            childOffset
        }
    }
}

export const getVerticalTooltipCoords = (placement: TPlacement, props: IGetCoordsProps): IReturn => {
    if (placement !== 'top' && placement !== 'bottom') {
        throw new Error('Неверное использование функции getVerticalTooltipCoords');
    }

    let {
        resultPosition,
        arrowPosition,
        topOffset,
        leftOffset,
        arrowConfig: {
            arrowOffset,
            arrowHalfSize,
            arrowSideSize
        },
        tooltipSizes: {
            tooltipBodyWidth,
            tooltipBodyHeightWithArrow,
            childOffset
        },
        childRect: {
            childHeight,
            childHalfWidth,
            childHalfHeight,
            childWidth,
            topSpace,
            leftSpace,
        }
    } = getInitialValues(placement, props);

    const topBaseOffset = topSpace - tooltipBodyHeightWithArrow;
    const negativeArrowHalfSize = -arrowHalfSize;

    if (placement === 'top') {
        if (topSpace < tooltipBodyHeightWithArrow) {
            resultPosition = 'bottom';
        } else {
            resultPosition = 'top';
        }
    } else {
        const bottomSpace = window.innerHeight - topSpace - childHeight;

        if (bottomSpace >= tooltipBodyHeightWithArrow) {
            resultPosition = 'bottom';
        } else {
            resultPosition = 'top';
        }
    }

    if (resultPosition === 'bottom') {
        topOffset = topSpace + childHeight + arrowOffset + window.scrollY;
        arrowPosition.top = negativeArrowHalfSize;
    } else {
        topOffset = topBaseOffset + window.scrollY;
        arrowPosition.bottom = negativeArrowHalfSize;
    }

    arrowPosition.left = `calc(50% - ${arrowHalfSize}px)`;

    if (leftSpace <= childOffset) {
        leftOffset = window.scrollX;
        arrowPosition.left = leftSpace + childHalfWidth - arrowHalfSize;
        if(arrowPosition.left <= 0){
            arrowPosition.left = arrowHalfSize
        }
    } else {
        const documentWidth = window.innerWidth;

        if (leftSpace + childWidth + childOffset > documentWidth) {
            leftOffset = documentWidth - tooltipBodyWidth + window.scrollX;
            const rightSpace = documentWidth - (leftSpace + childWidth + childOffset);

            arrowPosition.left = childHalfWidth + childOffset - arrowHalfSize - rightSpace
            if(arrowPosition.left >= tooltipBodyWidth - arrowHalfSize){
                arrowPosition.left = tooltipBodyWidth - arrowSideSize - arrowHalfSize
            };
        } else {
            leftOffset = leftSpace - childOffset + window.scrollX;
        }

    }

    return {
        leftOffset,
        topOffset,
        position: resultPosition,
        arrow: arrowPosition,
    }
}

const getHorizonTooltipCoords = (placement: TPlacement, props: IGetCoordsProps): IReturn => {
    if (placement !== 'left' && placement !== 'right') {
        throw new Error('Неверное использование функции getHorizonTooltipCoords');
    }

    let {
        resultPosition,
        arrowPosition,
        topOffset,
        leftOffset,
        arrowConfig: {
            arrowSideSize,
            arrowOffset,
            arrowHalfSize,
        },
        tooltipSizes: {
            tooltipBodyWidthWithArrow,
            tooltipBodyHeight,
            childOffset
        },
        childRect: {
            childHeight,
            childHalfHeight,
            childWidth,
            topSpace,
            leftSpace,
        }
    } = getInitialValues(placement, props);

    if (placement === 'left') {
        if (leftSpace <= tooltipBodyWidthWithArrow) {
            resultPosition = 'right';
        } else {
            resultPosition = 'left';
        }
    } else {
        const rightSpace = window.innerWidth - leftSpace - childWidth;

        if (rightSpace <= tooltipBodyWidthWithArrow) {
            resultPosition = 'left';
        } else {
            resultPosition = 'right';
        }
    }

    if (resultPosition === 'left') {
        leftOffset = leftSpace - tooltipBodyWidthWithArrow + window.scrollX;
        arrowPosition.right = -arrowHalfSize;
    } else {
        leftOffset = leftSpace + childWidth + arrowOffset + window.scrollX;
        arrowPosition.left = -arrowHalfSize;
    }

    arrowPosition.top = `calc(50% - ${arrowHalfSize}px)`;

    if (topSpace <= childOffset) {
        topOffset = window.scrollY;

        arrowPosition.top = topSpace + childHalfHeight - arrowHalfSize
        if(arrowPosition.top <= 0){
            arrowPosition.top = arrowHalfSize
        }
    } else {
        topOffset = topSpace - childOffset + window.scrollY;

        const bottomSpace = window.innerHeight - topSpace - childHeight;

        if (bottomSpace <= childOffset) {
            topOffset = window.innerHeight - tooltipBodyHeight + window.scrollY;

            const contentTopOffsetOfChild = tooltipBodyHeight - childHeight - bottomSpace;

            arrowPosition.top = contentTopOffsetOfChild + childHalfHeight - arrowHalfSize;
            if(arrowPosition.top >= tooltipBodyHeight - arrowHalfSize){
                arrowPosition.top = tooltipBodyHeight - arrowSideSize - arrowHalfSize;
            }
        } else {
            topOffset = topSpace - childOffset + window.scrollY;
        }
    }

    return {
        leftOffset,
        topOffset,
        position: resultPosition,
        arrow: arrowPosition,
    }
}

const getTopPlacementCoords = (props: IGetCoordsProps): IReturn => {
    return getVerticalTooltipCoords('top', props);
}

const getBottomPlacementCoords = (props: IGetCoordsProps): IReturn => {
    return getVerticalTooltipCoords('bottom', props);
}

const getLeftPlacementCoords = (props: IGetCoordsProps): IReturn => {
    return getHorizonTooltipCoords('left', props);
}

const getRightPlacementCoords = (props: IGetCoordsProps): IReturn => {
    return getHorizonTooltipCoords('right', props)
}

export const CoordsHelper: Record<TPlacement, (props: IGetCoordsProps) => IReturn> = {
    top: getTopPlacementCoords,
    bottom: getBottomPlacementCoords,
    left: getLeftPlacementCoords,
    right: getRightPlacementCoords,
}
