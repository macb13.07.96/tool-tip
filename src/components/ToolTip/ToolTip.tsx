import React, {DOMAttributes, ReactElement, ReactNode, useRef, useState} from "react";
import {CoordsHelper} from "./utils";
import {createPortal} from "react-dom";
import classNames from "classnames";
import {ICoords, IReturn, TPlacement} from "./types";


interface ITooltipProps {
    title: ReactNode;
    description: ReactNode;
    children: ReactElement<DOMAttributes<HTMLElement>>;
    placement?: TPlacement;
    showDelay?: number;
    hideDelay?: number;
}

const Tooltip: React.FC<ITooltipProps> = ({
                                              title,
                                              description,
                                              children,
                                              placement = 'top',
                                              showDelay = 0,
                                              hideDelay = 0
                                          }) => {
    // State to manage tooltip visibility
    const [position, setPosition] = useState<ITooltipProps['placement']>(placement);
    const [showTooltip, setShowTooltip] = useState(false);
    const [coords, setCoords] = useState<null | ICoords>(null);

    const containerRef = useRef<HTMLDivElement>(null);
    const contentRef = useRef<HTMLDivElement>(null);
    const arrowRef = useRef<HTMLDivElement>(null);
    const timeoutRef = useRef<{ timeoutId: NodeJS.Timeout | null }>({timeoutId: null})

    const handleOpen = () => {
        const child: HTMLElement = containerRef.current?.firstChild as HTMLElement;
        const content = contentRef.current;
        const arrow = arrowRef.current;
        if (!child || !content || !arrow) return;

        const result: IReturn = CoordsHelper[placement]({
            child,
            content,
            arrow
        });

        setCoords({
            top: result.topOffset,
            left: result.leftOffset,
            arrow: result.arrow,
        });
        setPosition(result.position);

        if (showDelay === 0) {
            setShowTooltip(true);
            return;
        }

        if (timeoutRef.current.timeoutId) {
            clearTimeout(timeoutRef.current.timeoutId);
        }

        timeoutRef.current.timeoutId = setTimeout(() => {
            setShowTooltip(true);
        }, showDelay)
    }

    const handleHide = () => {
        let hideDelayMs = hideDelay;
        if (hideDelay <= 50) {
            hideDelayMs = 50;
        }

        if (timeoutRef.current.timeoutId) {
            clearTimeout(timeoutRef.current.timeoutId);
        }

        timeoutRef.current.timeoutId = setTimeout(() => {
            setShowTooltip(false);
        }, hideDelayMs);
    }

    const handleKeepOpen = () => {
        setShowTooltip(true);
        if (timeoutRef.current.timeoutId) {
            clearTimeout(timeoutRef.current.timeoutId);
        }
    }


    return (
        <div
            ref={containerRef}
            className="flex flex-col gap-5 items-center justify-center relative"
        >
            {React.cloneElement(children, {
                onMouseEnter: handleOpen,
                onMouseLeave: handleHide
            })}

            {createPortal(
                <div
                    style={{
                        top: coords?.top,
                        left: coords?.left
                    }}
                    onMouseEnter={handleKeepOpen}
                    onMouseLeave={handleHide}
                    className={classNames('z-50 w-72 absolute transition-all opacity-0 pointer-events-none border-solid border border-stone-300 bg-white rounded-md', {'opacity-100 pointer-events-auto': showTooltip})}
                    ref={contentRef}
                >
                    <div className='opacity-100 p-3 text-xs font-normal text-center'>
                        {React.isValidElement(title) ? title : (
                            <h6 className="text-xl font-bold">
                                {title}
                            </h6>
                        )}
                        {React.isValidElement(description) ? description : (
                            <span className="text-xs text-gray-800">
                                {description}
                            </span>
                        )}

                    </div>
                    <div
                        ref={arrowRef}
                        className={classNames('-z-10 w-5 h-5 bg-white border-solid border border-stone-300 absolute rotate-45', {
                            'border-l-0 border-b-0': position === 'left',
                            'border-r-0 border-b-0': position === 'bottom',
                            'border-l-0 border-t-0': position === 'top',
                            'border-r-0 border-t-0': position === 'right',
                        })}
                        style={{
                            left: coords?.arrow.left,
                            top: coords?.arrow.top,
                            right: coords?.arrow.right,
                            bottom: coords?.arrow.bottom
                        }}
                    />
                </div>,
                document.getElementById('root') as HTMLDivElement
            )}

        </div>
    );
};

export default Tooltip;
