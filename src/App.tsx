import React from 'react';
import Tooltip from "./components/ToolTip/ToolTip";

function App() {
    return (
        <div className="h-screen flex items-start justify-start bg-cyan-50 pl-96">
            <Tooltip
                title="Хоккей: Игра и Вдохновение"
                description="Хоккей – это не просто спорт, это искусство, воплощенное на льду. От молодых амбициозных игроков до легенд, чьи имена стали символами этого замечательного вида спорта, хоккей объединяет людей вокруг мира."
                placement="top"
                showDelay={300}
                hideDelay={0}
            >
                <button
                    className="ml-24 text-base font-medium text-white bg-sky-400 border-0 rounded-md py-2.5 px-6 cursor-pointer">Pay
                    Now
                </button>
            </Tooltip>
        </div>
    );
}

export default App;
